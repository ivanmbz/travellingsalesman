import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ivanm on 08.12.2016.
 */
public class GUI extends JFrame{
    private Dimension bs = Toolkit.getDefaultToolkit().getScreenSize(); //Fenstergröße
    private JPanel panel = null; //Menüleiste
    private JButton btn_berechne = null;
    private JButton btn_loeschen = null;
    private JLabel lbl_slider = null;
    private JSlider slider = null;
    private JButton btn_colorDraw = null;
    private JColorChooser colorDraw = null;
    private JButton btn_colorCalc = null;
    private JColorChooser colorCalc = null;
    private JCheckBox chk_verbinde = null;
    private JCheckBox chk_linie = null;
    private JLabel lbl_output = null;
    private JEditorPane txt_output = null;

    private int anzahlBerechnungen = 100; //Anzahl der durchzuführenden Berechnungen
    private Color farbeZeichnen = Color.RED; //Farbe der Linie, welche die vom Benutzer definierte Route zeichnet.
    private Color farbeBerechnen = Color.BLACK; //Farbe der Linie, welche die berechnete Route zeichnet.
    private boolean verbindePunkte = true; //Die Punkte sollen vor der Berechnung schon verbunden werden.
    private boolean zeichneLinie = true; //Die Punkte sollen nach der Berechnung durch eine Linie verbunden werden.

    public GUI(){
        setLayout(null);
        setBounds(bs.width/2-500, bs.height/2-350,1000,700);
        setTitle("Travelling Salesman");
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH); //Maximiert
        this.getContentPane().setBackground(Color.GRAY);


        //JPanel
        panel = new JPanel(new BorderLayout());
        panel.setBounds(0, 0, 200, bs.height);
        panel.setBackground(Color.DARK_GRAY);
        panel.setLayout(null);
        this.getContentPane().add(panel);


        //Button Berechnen
        btn_berechne = new JButton();
        btn_berechne.setBounds(10, 10, 180, 30);
        btn_berechne.setText("Berechne");
        btn_berechne.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO
            }
        });


        //Button Löschen
        btn_loeschen = new JButton();
        btn_loeschen.setBounds(10, 50, 180, 30);
        btn_loeschen.setText("Löschen");
        btn_loeschen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO
            }
        });


        //Slider zur Auswahl der Anzahl der durchzuführenden Berechnungen
        slider = new JSlider();
        slider.setBounds(10, 130, 180, 30);
        slider.setToolTipText(String.valueOf(slider.getValue()));
        slider.setBackground(Color.DARK_GRAY);
        slider.setMinimum(100); //Es können Werte zw. 100 und 1000 ausgewählt werden
        slider.setMaximum(1000);
        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                slider.setToolTipText(String.valueOf(slider.getValue()));
            }
        });

        lbl_slider = new JLabel();
        lbl_slider.setBounds(10, 110, 180, 30);
        lbl_slider.setText("Anzahl der Berechnungen");
        lbl_slider.setForeground(Color.BLACK);


        //Farbe der gezeichneten Linie
        btn_colorDraw = new JButton();
        btn_colorDraw.setBounds(10, 190, 180, 30);
        btn_colorDraw.setText("Linienfarbe ZEICHNEN");
        btn_colorDraw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                colorDraw = new JColorChooser();
                colorDraw.showDialog(GUI.this, "Farbe auswählen", Color.RED);
                setFarbeZeichnen(colorDraw.getColor());
            }
        });


        //Farbe der berechneten Linie
        btn_colorCalc = new JButton();
        btn_colorCalc.setBounds(10, 230, 180, 30);
        btn_colorCalc.setText("Linienfarbe BERECHNEN");
        btn_colorCalc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                colorCalc = new JColorChooser();
                colorCalc.showDialog(GUI.this, "Farbe auswählen", Color.BLACK);
                setFarbeBerechnen(colorCalc.getColor());
            }
        });


        //CheckBox --> Punkte verbinden
        chk_verbinde = new JCheckBox();
        chk_verbinde.setBounds(10, 300, 180, 30);
        chk_verbinde.setText("Punkte verbinden");
        chk_verbinde.setForeground(Color.BLACK);
        chk_verbinde.setSelected(true);
        chk_verbinde.setBackground(Color.DARK_GRAY);
        chk_verbinde.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (chk_verbinde.isSelected())
                    setVerbindePunkte(true);
                else{
                    setVerbindePunkte(false);
                }
            }
        });


        //CheckBox --> Linie nach der Berechnung zeichnen
        chk_linie = new JCheckBox();
        chk_linie.setBounds(10, 340, 180, 30);
        chk_linie.setText("Linie nach der Berechnung zeichnen");
        chk_linie.setForeground(Color.BLACK);
        chk_linie.setSelected(true);
        chk_linie.setBackground(Color.DARK_GRAY);
        chk_linie.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (chk_linie.isSelected())
                    setZeichneLinie(true);
                else{
                    setZeichneLinie(false);
                }
            }
        });

        //Ausgabe der Routedaten
        lbl_output = new JLabel();
        lbl_output.setBounds(10, 390, 180, 30);
        lbl_output.setText("Routedaten:");
        lbl_output.setForeground(Color.BLACK);

        txt_output = new JEditorPane();
        txt_output.setBounds(10, 420, 180, 230);
        txt_output.setEditable(false);
        txt_output.setBackground(Color.GRAY);


        //Elemente der Menüleiste hinzufügen
        panel.add(btn_berechne);
        panel.add(btn_loeschen);
        panel.add(lbl_slider);
        panel.add(slider);
        panel.add(btn_colorDraw);
        panel.add(btn_colorCalc);
        panel.add(chk_verbinde);
        panel.add(chk_linie);
        panel.add(lbl_output);
        panel.add(txt_output);

        setVisible(true);
    }

    public int getAnzahlBerechnungen() {
        return anzahlBerechnungen;
    }

    public void setAnzahlBerechnungen(int anzahlBerechnungen) {
        this.anzahlBerechnungen = anzahlBerechnungen;
    }

    public Color getFarbeZeichnen() {
        return farbeZeichnen;
    }

    public void setFarbeZeichnen(Color farbeZeichnen) {
        this.farbeZeichnen = farbeZeichnen;
    }

    public Color getFarbeBerechnen() {
        return farbeBerechnen;
    }

    public void setFarbeBerechnen(Color farbeBerechnen) {
        this.farbeBerechnen = farbeBerechnen;
    }

    public boolean isVerbindePunkte() {
        return verbindePunkte;
    }

    public void setVerbindePunkte(boolean verbindePunkte) {
        this.verbindePunkte = verbindePunkte;
    }

    public boolean isZeichneLinie() {
        return zeichneLinie;
    }

    public void setZeichneLinie(boolean zeichneLinie) {
        this.zeichneLinie = zeichneLinie;
    }

    /**
     * Die Methode main startet das Programm, indem sie ein neues Fenster öffnet.
     * @param args
     */
    public static void main(String[] args){
        GUI gui = new GUI();
    }
}
